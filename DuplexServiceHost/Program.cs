﻿using DuplexServiceLibraty;
using System;

using System.ServiceModel;
using System.ServiceModel.Description;


namespace DuplexServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:8001/DuplexCalcService/");


            ServiceHost selfHost = new ServiceHost(typeof(DuplexCalc), baseAddress);

            try
            {

                selfHost.AddServiceEndpoint(typeof(IDuplexCalc), new WSDualHttpBinding(), "CalculatorService");

                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                selfHost.Description.Behaviors.Find<ServiceDebugBehavior>().IncludeExceptionDetailInFaults = true;
                selfHost.Description.Behaviors.Add(smb);


                selfHost.Open();
                Console.WriteLine("Serwis działa....");
                Console.WriteLine("Naciśnij <ENTER> by zakończyć.");
                Console.WriteLine();
                Console.ReadLine();

                // zamykamy serwis
                selfHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Przechwyciłem wyjątek: {0}", ce.Message);
                selfHost.Abort();
            }


        }
    }
}
