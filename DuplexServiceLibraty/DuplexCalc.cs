﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace DuplexServiceLibraty
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Single)]
    public class DuplexCalc : IDuplexCalc
    {

        double wynik = 0.0D;
        string rownanie;
        bool stop = false;
        System.Timers.Timer myTimer = new System.Timers.Timer();

        IDuplexCalcCallback callback = OperationContext.Current.GetCallbackChannel<IDuplexCalcCallback>();

        int licznik = 0;
        public DuplexCalc()
        {
            Thread thread1 = new Thread(new ThreadStart(this.Idk));
            rownanie = wynik.ToString();
            
            
            thread1.Start();
            

        }
        public void Wyczysc()
        {
            rownanie = wynik.ToString();
        }

        public void AddTo(double n1)
        {
            wynik += n1;
            //Task.Run(() => Idk());
            Console.WriteLine("Dodawanie liczb: Add({0})", n1);
            Console.WriteLine("Wynik: {0}", wynik);
            Callback.Wynik(wynik);
            if(myTimer.Enabled == false)
            {
                myTimer.Start();
            }
        }

        public void SubtractFrom(double n1)
        {
            wynik -= n1;
            Console.WriteLine("Odejmowanie liczb: Subtract({0})", n1);
            Console.WriteLine("Wynik: {0}", wynik);
            Callback.Wynik(wynik);

            if (myTimer.Enabled == false)
            {
                myTimer.Start();
            }
        }

        public void MultiplyBy(double n1)
        {
            wynik *= n1;
            Console.WriteLine("Mnożenie liczba: Multiply({0})", n1);
            Console.WriteLine("Wynik: {0}", wynik);
            Callback.Wynik(wynik);

            if (myTimer.Enabled == false)
            {
                myTimer.Start();
            }
        }

        public void DivideBy(double n1)
        {
            wynik = wynik / n1;
            Console.WriteLine("Dzielenie liczb: Divide({0})", n1);
            Console.WriteLine("Wynik: {0}", wynik);
            Callback.Wynik(wynik);

            if (myTimer.Enabled == false)
            {
                myTimer.Start();
            }

        }

        public void licznikStop()
        {
            myTimer.Stop();
        }

        public void Idk()
        {
            
            myTimer.Elapsed += new ElapsedEventHandler(DisplayTimeEvent);
            myTimer.Interval = 1000;
            myTimer.AutoReset = true;
            myTimer.Enabled = true;
            GC.KeepAlive(myTimer);
        }



        public void DisplayTimeEvent(object source, ElapsedEventArgs e)
        {
            licznik++;
            //Callback not = (Callback)s
            Console.WriteLine("Licznik" + licznik.ToString());


            callback.Timer(licznik);

            //Callback.Timer(licznik);
        }


        IDuplexCalcCallback Callback
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IDuplexCalcCallback>();
            }
        }
    }
}
