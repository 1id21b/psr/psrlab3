﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Timers;

namespace DuplexServiceLibraty
{

    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IDuplexCalcCallback))]
    public interface IDuplexCalc
    {
        [OperationContract]
        void Wyczysc();
        [OperationContract]
        void AddTo(double n1);
        [OperationContract]
        void SubtractFrom(double n1);
        [OperationContract]
        void MultiplyBy(double n1);
        [OperationContract]
        void DivideBy(double n1);
        [OperationContract]
        void licznikStop();

    }

    public interface IDuplexCalcCallback
    {
        [OperationContract(IsOneWay = true)]
        void Wynik(double result);
        [OperationContract(IsOneWay = true)]
        void Rownanie(string eqn);
        [OperationContract(IsOneWay = true)]
        void Timer(int t);

    }
}
