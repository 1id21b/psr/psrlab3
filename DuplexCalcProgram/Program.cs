﻿using DuplexCalcProgram.DuplexCalcServiceRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DuplexCalcProgram
{
    //public class CallbackHandler : IDuplexCalcCallback
    //{
    //    public void Wynik(double result)
    //    {
    //        Console.WriteLine("Wynik({0})", result);
    //    }

    //    public void Rownanie(string eqn)
    //    {
    //        Console.WriteLine("Równanie({0})", eqn);
    //    }
    //    public void Timer(int t)
    //    {
    //        Console.WriteLine("Licznik({0})", t);
    //    }
    //}
    class Program : IDuplexCalcCallback
    {
        public void Wynik(double result)
        {
            Console.WriteLine("Wynik({0})", result);
        }

        public void Rownanie(string eqn)
        {
            Console.WriteLine("Równanie({0})", eqn);
        }
        public void Timer(int t)
        {
            Console.WriteLine("Licznik({0})", t);
        }

        public void zawolajSerwis()
        {
            InstanceContext instanceContext = new InstanceContext(this);

            DuplexCalcClient client = new DuplexCalcClient(instanceContext);
            
            client.Open();
            client.AddTo(3.0);
            client.MultiplyBy(2.0);
            client.DivideBy(4.0);
            client.SubtractFrom(1);
            client.Wyczysc();
            Console.ReadLine();
            client.licznikStop();
            Console.ReadLine();
            client.AddTo(7.0);
            client.MultiplyBy(1.5);
            Console.ReadLine();

            client.licznikStop();
            client.Close();
        }

        static void Main(string[] args)
        {
            Program pro = new Program();
            pro.zawolajSerwis();
        }
    }

}
